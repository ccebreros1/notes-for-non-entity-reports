package chinook.service;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import chinook.entity.Playlist;
import chinook.entity.PlaylistTrack;
import chinook.entity.PlaylistTrackPK;
import chinook.entity.Track;
import chinook.exception.IllegalGenreException;

@Stateful
public class PlaylistService {

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	public void add(Playlist playlist) {
		entityManager.persist(playlist);
	}
	
	public Playlist findOneByPlaylistId(int playlistId) {
		return entityManager.find(Playlist.class, playlistId);
	}
	
	@SuppressWarnings("unchecked")
	public List<Playlist> findAll() {
		return entityManager.createNamedQuery("Playlist.findAll").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Playlist> findAllOrderByName() {
		return entityManager.createQuery("FROM Playlist ORDER BY name").getResultList();
	}

	
	public void update(Playlist playlist) {
		Playlist updatedPlaylist = entityManager.merge(playlist);
		entityManager.persist(updatedPlaylist);
	}
	
	public void delete(Playlist playlist) {
		entityManager.remove(playlist);
	}
	
	@Resource
	private SessionContext context; //This creates a transaction
	
	public void createNew(String playlistName, List<Track> playlistTracks) throws IllegalGenreException
	{
		//create a new playlist entity and persist the entity
		
		Playlist currentPlaylist = new Playlist();
		currentPlaylist.setName(playlistName);
		entityManager.persist(currentPlaylist);
		
		//for each Track in playlistTracks create a
		for (Track currentTrack : playlistTracks)
		{
			//	rollback transaction if genre is Blues
			if (currentTrack.getGenre().getName().equalsIgnoreCase("Blues"))
			{
				context.getRollbackOnly();
				String message = String.format("Sorry I am not in a mood for %s", currentTrack.getGenre().getName());
				throw new IllegalGenreException(message);
			}
//			new PlaylistTrack and persist the entity
			else
			{
				PlaylistTrackPK id = new PlaylistTrackPK();
				id.setPlaylistId(currentPlaylist.getPlaylistId());
				id.setTrackId(currentTrack.getTrackId());
				PlaylistTrack currentPlaylistTrack = new PlaylistTrack();
				currentPlaylistTrack.setId(id);
				
			}
			
		}
		
	}
}
