package chinook.service;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import chinook.entity.Track;

@Stateful
public class TrackService {
	
	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Track> findAll() {
		return entityManager.createNamedQuery("Track.findAll").getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Track> findAllOrderByName() {
		Query query = entityManager.createQuery("FROM Track ORDER BY name");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Track> findAllByAlbumIdOrderByName(int albumId) {
		Query query = entityManager.createQuery("FROM Track WHERE album.albumId = :albumIdValue ORDER BY name");
		query.setParameter("albumIdValue", albumId);
		return query.getResultList();
	}

}
