package chinook.service;

import java.util.List;

import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;

import chinook.entity.Album;

@Stateful
public class AlbumService {

	@PersistenceContext(type=PersistenceContextType.EXTENDED)
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	public List<Album> findAll() {
		Query query = entityManager.createNamedQuery("Album.findAll");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Album> findAllOrderByTitle() {
		Query query = entityManager.createQuery("FROM Album ORDER BY title");
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Album> findAllByArtistIdOrderByTitle(int artistId) {
		Query query = entityManager.createQuery("FROM Album WHERE artistId = :artistIdValue ORDER BY title");
		query.setParameter("artistIdValue", artistId);
		return query.getResultList();
	}
	
	public Album findOneByAlbumId(int albumId) {
		return entityManager.find(Album.class, albumId);
	}
	
	public void update(Album currentAlbum) {
		Album albumUpdated = entityManager.merge(currentAlbum);
		entityManager.persist(albumUpdated);
	}
	
	public void add(Album currentAlbum) {
		entityManager.persist(currentAlbum);
	}
	
	public void delete(Album currentAlbum) {
		entityManager.remove(currentAlbum);
	}

	
}
