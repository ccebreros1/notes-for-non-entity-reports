package chinook.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the PlaylistTrack database table.
 * 
 */
@Embeddable
public class PlaylistTrackPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name="PlaylistId", insertable=false, updatable=false)
	private int playlistId;

	@Column(name="TrackId", insertable=false, updatable=false)
	private int trackId;

	public PlaylistTrackPK() {
	}
	public int getPlaylistId() {
		return this.playlistId;
	}
	public void setPlaylistId(int playlistId) {
		this.playlistId = playlistId;
	}
	public int getTrackId() {
		return this.trackId;
	}
	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof PlaylistTrackPK)) {
			return false;
		}
		PlaylistTrackPK castOther = (PlaylistTrackPK)other;
		return 
			(this.playlistId == castOther.playlistId)
			&& (this.trackId == castOther.trackId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.playlistId;
		hash = hash * prime + this.trackId;
		
		return hash;
	}
}