package chinook.controller;

import java.io.IOException;
import java.util.List;

import javax.enterprise.inject.Model;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import chinook.entity.Album;
import chinook.entity.Artist;
import chinook.service.AlbumService;
import chinook.service.ArtistService;
import helper.JSFHelper;

@Model
public class AlbumByArtistController {

	@Inject
	private AlbumService albumService;
	
	@Inject
	private ArtistService artistService;
	
	private List<Album> albumsByArtist;
	
	private int artistId;

	private Artist selectedArtist;
	
	public int getArtistId() {
		return artistId;
	}
	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}
	public List<Album> getAlbumsByArtist() {
		return albumsByArtist;
	}
	public Artist getSelectedArtist() {
		return selectedArtist;
	}

	public void findAlbumsByArtistId() throws IOException {
		if (artistId <= 0) {
			JSFHelper.addErrorMessage("Bad request. Please use a link from within the system.");
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			FacesContext.getCurrentInstance().responseComplete();
			ec.redirect(ec.getRequestContextPath() + "/pages/chinook/artists.xhtml");
		} else {
			selectedArtist = artistService.findOneByArtistId(artistId);
			if (selectedArtist == null ) {
				String message = String.format("Bad request. Unknown artistId %d", artistId);
				JSFHelper.addErrorMessage(message);
			} else {
//				albumsByArtist = albumService.findAllByArtistIdOrderByTitle(artistId);
				albumsByArtist = selectedArtist.getAlbums();
				if( albumsByArtist == null ) {
					String message = String.format("Bad request. Unknown artistId %d", artistId);
					JSFHelper.addErrorMessage(message);
				} else if( albumsByArtist == null || albumsByArtist.size() == 0 ) {
					String message = String.format("There are no albums for artistId %d", artistId);
					JSFHelper.addInfoMessage(message);
				}
			}			
		}
	}
	
}
