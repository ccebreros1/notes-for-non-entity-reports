package chinook.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.HorizontalBarChartModel;

import chinook.domain.GenreRevenue;
import chinook.service.GenreService;

@Named			
@ViewScoped
public class GenreRevenueReportController implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	private GenreService genreService;
	
	private List<GenreRevenue> genreRevenueList;	// getter	
	private HorizontalBarChartModel revenueChartModel;	// getter

	public List<GenreRevenue> getGenreRevenueList() {
		return genreRevenueList;
	}
	public HorizontalBarChartModel getRevenueChartModel() {
		return revenueChartModel;
	}

	@PostConstruct
	public void init() {
		genreRevenueList = genreService.genreRevenues();
		revenueChartModel = new HorizontalBarChartModel();
		ChartSeries revenueSeries = new ChartSeries();
		for(GenreRevenue revenue : genreRevenueList ) {
			revenueSeries.set(revenue.getGenreName(), 
					revenue.getGenreTotalRevenue());
		}
		revenueChartModel.addSeries(revenueSeries);
		revenueChartModel.setTitle("Total Revenue By Genre");
	}
	
}
