package chinook.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import chinook.entity.Album;
import chinook.entity.Artist;
import chinook.service.AlbumService;
import chinook.service.ArtistService;
import helper.JSFHelper;

@Named	// allows you use access this object using JSF-EL 
		// #{objectName.propertyName} OR #{objectName.methodName()}
@ViewScoped	// must implement java.io.Serializable
public class FindAlbumController implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotNull(message="Album Id value is required.")
	private Integer searchValue;	// getter/setter
	
	private Album currentAlbum;		// getter
	
	private List<Artist> artists;	// getter
	
	@Min(value=1, message="An valid artist must be selected")
	private int selectedArtistId;		// getter/setter
	
	@Inject
	private ArtistService artistService;
	
	@Inject
	private AlbumService albumService;
		
	@PostConstruct
	public void init() {
		try {
			artists = artistService.findAllOrderByName();
		} catch( Exception e ) {
			JSFHelper.addFatalMessage("Error retreiving artists");
		}
	}
	
	public void findAlbum() {
		int albumId = searchValue;
		currentAlbum = albumService.findOneByAlbumId(albumId);
		selectedArtistId = currentAlbum.getArtist().getArtistId();
		if( currentAlbum == null ) {
			JSFHelper.addErrorMessage("No record found.");
		} else {
			JSFHelper.addInfoMessage("Found 1 record.");
		}
	}
	
	public void saveAlbum() {
		// find an Artist with the selectedArtistId value
		Artist currentArtist 
			= artistService.findOneByArtistId(selectedArtistId);
		currentAlbum.setArtist(currentArtist);
		albumService.update(currentAlbum);
		JSFHelper.addInfoMessage("Successfully updated information");
	}
	
	public void cancel() {
		currentAlbum = null;
		selectedArtistId = 0;
	}
	
	public void deleteAlbum() {
		try {
			albumService.delete(currentAlbum);
			JSFHelper.addInfoMessage("Successfully deleted all album information");
			cancel();
		} catch( Exception e ) {
			JSFHelper.addErrorMessage("This album cannot be deleted");
		}
	}

	public Integer getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(Integer searchValue) {
		this.searchValue = searchValue;
	}

	public Album getCurrentAlbum() {
		return currentAlbum;
	}

	public List<Artist> getArtists() {
		return artists;
	}

	public int getSelectedArtistId() {
		return selectedArtistId;
	}

	public void setSelectedArtistId(int selectedArtistId) {
		this.selectedArtistId = selectedArtistId;
	}
	
}