package chinook.controller;

import java.util.List;

import javax.enterprise.inject.Model;
import javax.inject.Inject;

import chinook.entity.Track;
import chinook.service.TrackService;

@Model
public class TrackController {

	@Inject
	private TrackService trackService;
	
	public List<Track> retrieveAllTracks() {
		return trackService.findAllOrderByName();
	}
}
