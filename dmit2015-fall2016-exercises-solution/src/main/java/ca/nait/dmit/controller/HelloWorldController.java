package ca.nait.dmit.controller;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.hibernate.validator.constraints.NotBlank;

import helper.JSFHelper;

/**
 * This is the CDI managed bean class that contains the properties
 * and event handler actions for the web page "helloWorld.xhtml".
 * 
 * @author Sam Wu
 * @version 2016.09.28
 * 
 */
@Named				// mark this class as a CDI managed bean named "helloWorldController"
@RequestScoped		// create the managed bean for one HTTP Request
public class HelloWorldController {
	
	@NotBlank(message="Please enter your name.")
	private String username;
	private int age;
		
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	public HelloWorldController() {

	}

	public void doSubmit() {
		String message = String.format("Hello %s and welcome to JSF World!",username);
		if( age < 18 ) {
			JSFHelper.addWarningMessage(message);
		} else if ( age >= 18 && age <= 35 ) {
			JSFHelper.addInfoMessage(message);
		} else if( age > 35 && age <= 65 ) {
			JSFHelper.addErrorMessage(message);
		} else {
			JSFHelper.addFatalMessage(message);
		}
	}
}
