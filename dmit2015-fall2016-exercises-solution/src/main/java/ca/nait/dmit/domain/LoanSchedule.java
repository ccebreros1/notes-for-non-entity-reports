package ca.nait.dmit.domain;

/**
 * This entity class is use to track the interest paid, principal paid, and
 * balance remaining for each payment in the life of a loan.
 *
 * @author Sam Wu
 * @version 2010.09.20
 */
public class LoanSchedule
{
  /** the payment number of the loan */
  private int paymentNumber;
  /** the interest paid portion of the monthly payment amount */
  private double interestPaid;
  /** the principal paid portion of the monthly payment amount */
  private double principalPaid;
  /** the balance due on the loan at the start of each payment period */
  private double remainingBalance;
  
  public LoanSchedule()
  {
  }

  public LoanSchedule(int paymentNumber, double interestPaid, double principalPaid,
	  double remainingBalance)
  {
	this.paymentNumber = paymentNumber;
	this.interestPaid = interestPaid;
	this.principalPaid = principalPaid;
	this.remainingBalance = remainingBalance;
  }

  public int getPaymentNumber()
  {
    return paymentNumber;
  }

  public void setPaymentNumber(int paymentNumber)
  {
    this.paymentNumber = paymentNumber;
  }

  public double getInterestPaid()
  {
    return interestPaid;
  }

  public void setInterestPaid(double interestPaid)
  {
    this.interestPaid = interestPaid;
  }

  public double getPrincipalPaid()
  {
    return principalPaid;
  }

  public void setPrincipalPaid(double principalPaid)
  {
    this.principalPaid = principalPaid;
  }

  public double getRemainingBalance()
  {
    return remainingBalance;
  }

  public void setRemainingBalance(double remainingBalance)
  {
    this.remainingBalance = remainingBalance;
  }
}
